﻿using OpenQA.Selenium;
using SetupPageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupPieceObjects
{
    public class MainMenuPiece
    {
        //locators
        static string homeMenuButtonXpath = "//a[@href = '/home']";
        static string engagementsMenuButtonXpath = "//a[@href = '/engagements']";
        static string usersMenuButtonXpath = "//a[@href = '/users']";
        static string expandMenuButtonXpath = "//a[@href = '']";

        //po methods
        public static EngagementsPage ClickEngagementsIcon()
        {
            Driver.Instance.FindElement(By.XPath(engagementsMenuButtonXpath)).Click();
            EngagementsPage page = new EngagementsPage();
            Driver.IsOnCorrectPage("engagements");
            return page;
        }

        public static UsersPage ClickUsersIcon()
        {
            Driver.Instance.FindElement(By.XPath(usersMenuButtonXpath)).Click();
            UsersPage page = new UsersPage();
            Driver.IsOnCorrectPage("users");
            return page;
        }

        public static void ClickExpandIcon()
        {
            Driver.Instance.FindElement(By.XPath(expandMenuButtonXpath)).Click();
        }
    }
}
