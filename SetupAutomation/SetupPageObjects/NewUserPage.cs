﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SetupPageObjects
{
    public class NewUserPage:Driver
    {
        //locators
        static string usernameTextBoxCss = "#username";
        static string firstNameTextBoxCss = "#firstName";
        static string lastNameTextBoxCss = "#lastName";
        static string masterEngagementTextBoxCss = "input[name='masterEngagement']";
        static string masterEngagementDDLCss = "ul[id^='typeahead-'] li";
        static string saveButtonCss = "button[type='submit']";

        //User details
        static string titleTextBoxCss = "#title";
        static string employeeNumberTextBoxCss = "#employeeNumber";
        static string userInfoTextBoxCss = "#userInfo";
        static string timeZoneDDLCss = "select[name = 'timeZone']";
        static string cultureDDLCss = "select[name = 'culture']";
        static string languageDDLCss = "select[ng-model='user.Language']";
       
        static string accessAndRolesTabCss = "a[href='#engagementandapplications']";
        static string userDetailsTabCss = "a[href='#operatordetails']";
        static string addButtonCss = ".btn.a-item-btn.ng-scope";
        static string addEngagementTextBoxCss = "#engagementAdd_chosen";
        static string findEngagementTextBoxCss = "input[placeholder='Find engagement']";

        //Engagement and applications
        static string engagementNameButtonXpath = "//div[@id='engagementandapplications']/div/ul/li[1]/div[1]/p";
        static string sysAdminButtonXpath = "//div[@id='engagementandapplications']/div/ul/li[1]/div[1]/button[1]";
        static string balancerAccessButtonXpath = "//div[@id='engagementandapplications']/div/ul/li[1]/div[2]/ul/li[1]/div/button[1]";

        //Authentication
        static string authenticationTabCss = "a[href='#authentication']";

        //methods
        public void EnterUsername(string userName)
        {
            try
            {
                Driver.Instance.FindElement(By.CssSelector(usernameTextBoxCss)).SendKeys(userName);
            }
            catch (Exception e)
            {
                Console.Write("Error! in entering engagement name");
            }
        }

        public void EnterFirstName(string firstName)
        {
            Driver.Instance.FindElement(By.CssSelector(firstNameTextBoxCss)).SendKeys(firstName);
        }

        public void EnterLastName(string lastName)
        {
            Driver.Instance.FindElement(By.CssSelector(lastNameTextBoxCss)).SendKeys(lastName);
        }

        public void TypeAndSelectMasterEngagement(string masterEngagement)
        {
            Driver.Instance.FindElement(By.CssSelector(masterEngagementTextBoxCss)).SendKeys(masterEngagement);
            Thread.Sleep(5000);
            IWebElement ddl = Driver.Instance.FindElement(By.CssSelector(masterEngagementDDLCss));
            ddl.Click();
        }

        public void EnterAdditionalUserInformation(string title, string employeeNumber, string userInfo)
        {
            Driver.Instance.FindElement(By.CssSelector(titleTextBoxCss)).SendKeys(title);
            Driver.Instance.FindElement(By.CssSelector(employeeNumberTextBoxCss)).SendKeys(employeeNumber);
            Driver.Instance.FindElement(By.CssSelector(userInfoTextBoxCss)).SendKeys(userInfo);
        }

        public void SelectTimeZone(string timeZone)
        {
            IWebElement timeZoneDropDown = Driver.Instance.FindElement(By.CssSelector(timeZoneDDLCss));
            SelectElement selector = new SelectElement(timeZoneDropDown);
            selector.SelectByText(timeZone);
        }

        public void SelectCulture(string culture)
        {
            IWebElement cultureDropDown = Driver.Instance.FindElement(By.CssSelector(cultureDDLCss));
            SelectElement selector = new SelectElement(cultureDropDown);
            selector.SelectByText(culture);
        }
        public void SelectLanguage(string language)
        {
            IWebElement languageDropDown = Driver.Instance.FindElement(By.CssSelector(languageDDLCss));
            SelectElement selector = new SelectElement(languageDropDown);
            selector.SelectByText(language);
        }

        public void AddEngagementInEngagementAndApplications(string engagementName)
        {
            Driver.Instance.FindElement(By.CssSelector(accessAndRolesTabCss)).Click();
        }

        public void clickAdd()
        {
            Driver.Instance.FindElement(By.CssSelector(addButtonCss)).Click();
        }


        public EngagementsPage ClickSave()
        {
            try
            {
                Driver.Instance.FindElement(By.CssSelector(saveButtonCss)).Click();

            }
            catch (Exception e)
            {
                Console.Write("Error! in saving the engagement");
            }
            EngagementsPage page = new EngagementsPage();
            return page;
        }

        public void ClickOnTab(string tabName)
        {
            IWebElement tabElement;
            switch (tabName)
            {
                case "User details":
                    {
                        Driver.Instance.FindElement(By.CssSelector(userDetailsTabCss)).Click();
                        break;
                    }
                case "Access and roles":
                    {
                        Driver.Instance.FindElement(By.CssSelector(accessAndRolesTabCss)).Click();
                        break;
                    }
                case "Authentication":
                    {
                        Driver.Instance.FindElement(By.CssSelector(authenticationTabCss)).Click();
                        break;
                    }
            }
        }

        public void SetOverrideMasterEngagementSettingsON()
        {
            var j = Driver.Instance as IJavaScriptExecutor;
            String script = "return document.getElementsByClassName('bootstrap-switch-label')";
            IList<IWebElement> allBootstrapSwitches = (System.Collections.ObjectModel.ReadOnlyCollection<IWebElement>)j.ExecuteScript(script);
            //Clicking the fourth bootstrap switch in the list
            allBootstrapSwitches.ElementAt(3).Click();
        }
    }
}
