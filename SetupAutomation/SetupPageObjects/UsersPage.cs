﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupPageObjects
{
   public class UsersPage:Driver
    {
        //locators
        static string addUserButtonXpath = "//div[@id='mainview']/div[1]/button";

        //po methods
        public NewUserPage ClickAddUserButton()
        {
            try
            {
                Driver.Instance.FindElement(By.XPath(addUserButtonXpath)).Click();
            }

            catch(Exception e)
            {
                Console.Write("Unable to find or click Add Engagement");
            }
            NewUserPage page = new NewUserPage();
            Driver.IsOnCorrectPage("/users/new");
            return page;
        }
    }
}
