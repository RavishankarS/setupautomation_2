﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupPageObjects
{
    public class HomePage:Driver
    {
        //Locators
        static String homePageApplicationXpath = "//h3";
        static String successfulLoginsGraphCss = "#authChart svg g path";
        static String configuredUsersGraphCss = "#activeUsersChart svg g path";

        //Methods
        public static String GetApplicationName()
        {
            return Driver.Instance.FindElement(By.XPath(homePageApplicationXpath)).Text;
        }

        public static bool IsGraphPresent(String graphTitle)
        {
            if (graphTitle.Contains("Successful logins"))
            {
                WebDriverWait wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(15));
                IWebElement successfulLoginGraph = wait.Until(x => x.FindElement(By.CssSelector(successfulLoginsGraphCss)));
                    return successfulLoginGraph.Displayed;
            }
            else if (graphTitle.Contains("Configured users"))
            {
                WebDriverWait wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(15));
                IWebElement configuredUsersGraph = wait.Until(x => x.FindElement(By.CssSelector(configuredUsersGraphCss)));
                return configuredUsersGraph.Displayed;
            }
                return false;
        }
    }
}
