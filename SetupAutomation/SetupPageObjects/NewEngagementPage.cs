﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupPageObjects
{
    public class NewEngagementPage:Driver
    {
        //locators
        static string engagementNameTextBoxCss = "#engagementName";
        static string customerIdTextBoxCss = "#customerId";
        static string timeZoneDropdownCss = "select[name = 'timeZone']";
        static string balancerAppAccessControlCss = "input[type='checkbox'][value='balancer']";
        static string taskManagerAppAccessControlXpath = "(//div[@id = 'engagementdetails']//label[@class = 'a-label-checkbox'])[2]/div";
        static string balancerEditionTypeDropdownCss = "select[name = 'balancerEditionType']";
        static string balancerEditionDropdownCss = "select[name = 'balancerEdition']";
        static string taskManagerEditionTypeDropdownCss = "select[name = 'taskManagerEditionType']";
        static string taskManagerEditionDropdownCss = "select[name = 'taskManagerEdition']";
        static string activeControlCss = "input[type='checkbox'][value='active']";
        static string saveButtonCss = "button[type='submit']";
        static string defaultSecurityPolicyRadioCss = "input[value = 'defaultPolicy']";

        //Engagement and applications
        static string engagementNameButtonXpath = "//div[@id='engagementandapplications']/div/ul/li[1]/div[1]/p";
        static string sysAdminButtonXpath = "//div[@id='engagementandapplications']/div/ul/li[1]/div[1]/button[1]";
        static string balancerAccessButtonXpath = "//div[@id='engagementandapplications']/div/ul/li[1]/div[2]/ul/li[1]/div/button[1]";


        //methods
        public void EnterEngagementName(string engagementName)
        {
            try
            {
                Driver.Instance.FindElement(By.CssSelector(engagementNameTextBoxCss)).SendKeys(engagementName);
            }
            catch (Exception e)
            {
                Console.Write("Error! in entering engagement name");
            }
        }

        //Need to find out a number generator / way to delete the engagement after finishing the tests
        public void EnterCustomerId(int customerId)
        {
            Driver.Instance.FindElement(By.CssSelector(customerIdTextBoxCss)).SendKeys(customerId.ToString());
        }

        public void SelectTimeZone(string timeZone)
        {
            IWebElement timeZoneDropDown = Driver.Instance.FindElement(By.CssSelector(timeZoneDropdownCss));
            SelectElement selector = new SelectElement(timeZoneDropDown);
            selector.SelectByText(timeZone);
        }

        //Refactor the below method as to check if the control is on, before setting a value
        public void setAppAccess(string application)
        {
            if (application == "Balancer")
            {
                var j = Driver.Instance as IJavaScriptExecutor;
                String script = "return document.getElementsByClassName('bootstrap-switch-label')";
                IList<IWebElement> allBootstrapSwitches = (System.Collections.ObjectModel.ReadOnlyCollection<IWebElement>)j.ExecuteScript(script);
                foreach (IWebElement bootstrapSwitch in allBootstrapSwitches)
                {
                    bootstrapSwitch.Click();
                    break;
                }
            }
            else if (application == "Task Manager")
            {
                // TODO
            }

        }

        public void selectTypeAndEdition(string application, string editionType, string edition)
        {
            if (application == "Balancer")
            {
                IWebElement typeDropDown = Driver.Instance.FindElement(By.CssSelector(balancerEditionTypeDropdownCss));
                SelectElement editionTypeElement = new SelectElement(typeDropDown);
                editionTypeElement.SelectByText(editionType);

                IWebElement editionDropDown = Driver.Instance.FindElement(By.CssSelector(balancerEditionDropdownCss));
                SelectElement editionElement = new SelectElement(editionDropDown);
                editionElement.SelectByText(edition);
            }

            else if (application == "Task Manager")
            {
                IWebElement typeDropDown = Driver.Instance.FindElement(By.CssSelector(taskManagerEditionTypeDropdownCss));
                SelectElement editionTypeElement = new SelectElement(typeDropDown);
                editionTypeElement.SelectByText(editionType);

                IWebElement editionDropDown = Driver.Instance.FindElement(By.CssSelector(taskManagerEditionDropdownCss));
                SelectElement editionElement = new SelectElement(editionDropDown);
                editionElement.SelectByText(edition);
            }
        }

        // Need to check if this is already active or not
        public void setActive()
        {
            Driver.Instance.FindElement(By.CssSelector(activeControlCss)).Click();
        }

        public EngagementsPage clickSave()
        {
            try
            {
                Driver.Instance.FindElement(By.CssSelector(saveButtonCss)).Click();

            }
            catch (Exception e)
            {
                Console.Write("Error! in saving the engagement");
            }
            EngagementsPage page = new EngagementsPage();
            return page;
        }

    }
}
