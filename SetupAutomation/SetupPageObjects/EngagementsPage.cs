﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupPageObjects
{
    public class EngagementsPage:Driver
    {
        //locators
        static string addEngagementButtonCss = "button[ng-click=\"go(\'/engagements/new\')\"]";

        //po methods
        public NewEngagementPage ClickAddEngagementButton()
        {
            try
            {
                Driver.Instance.FindElement(By.CssSelector(addEngagementButtonCss)).Click();
            }

            catch (Exception e)
            {
                Console.Write("Unable to find or click Add Engagement");
            }
            NewEngagementPage page = new NewEngagementPage();
            Driver.IsOnCorrectPage("/engagements/new");
            return page;
        }
    }
}
