﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupPageObjects
{
    public class LoginPage:Driver
    {
        //Locators
        static String usernameId = "username";
        static String passwordId = "password";
        static String loginButtonXpath = "//input[@type = 'submit']";
        static String loginPageApplicationXpath = "//h1";

        //Methods
        public static void GoToUrl()
        {
            Driver.Instance.Manage().Cookies.DeleteAllCookies();
            Driver.Instance.Navigate().GoToUrl("https://setuptests.adramatch.com/");
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10));
            wait.Until(d => d.SwitchTo().ActiveElement().GetAttribute("id") == "username");
        }

        public void LoginAs(String userName, String password)
        {
            var txtUsername = Driver.Instance.FindElement(By.Id(usernameId));
            var txtPassword = Driver.Instance.FindElement(By.Id(passwordId));
            var buttonLogin = Driver.Instance.FindElement(By.XPath(loginButtonXpath));
            txtUsername.SendKeys(userName);
            txtPassword.SendKeys(password);
            buttonLogin.Click();
            Driver.Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
        }


        public static String GetApplicationName()
        {
            return Driver.Instance.FindElement(By.XPath(loginPageApplicationXpath)).Text;
        }
    }
}
