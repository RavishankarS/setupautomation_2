﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupPageObjects
{
    public class Driver
    {
        public static IWebDriver Instance { get; set; }

        public static void Initialise()
        {
            Instance = new FirefoxDriver();
            Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
        }

        public static void Shutdown()
        {
            Driver.Instance.Quit();
        }

        public static String GetTitle()
        {
            return Driver.Instance.Title;
        }

        public static bool IsOnCorrectPage(string pageUrlText)
        {
            if (Driver.Instance.Url.Contains(pageUrlText))
            {
                Console.Write("Verify Page Url Contains: " + pageUrlText + "");
                return true;
            }
            else
            {
                Console.Write("Page Url Contains: " + pageUrlText + " and does not match the expected");
                return false;
            }
        }
    }
}
