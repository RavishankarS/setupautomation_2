﻿using SetupPageObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupTests
{
    [TestFixture]
    public class HomePageTest:SetupBaseTest
    {
        [Test]
        public void verifyHomePage()
        {
            LoginPage loginPage = new LoginPage();
            loginPage.LoginAs("sa@99xt.com", "Intel@123");
            Assert.IsTrue(HomePage.IsGraphPresent("Successful logins"), "Successful logins graph is not displayed in home");
            Assert.IsTrue(HomePage.IsGraphPresent("Configured users"), "Configured users graph is not displayed in home");
        }
    }
}
