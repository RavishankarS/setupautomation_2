﻿using NUnit.Framework;
using SetupPageObjects;
using SetupPieceObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupTests
{
      [TestFixture]
    public class NewUserTest:SetupBaseTest
    {
        [Test]
        public void VerifyCreateSingleEngagementAdmin()
        {
            LoginPage loginPage = new LoginPage();
            loginPage.LoginAs("sa@99xt.com", "Intel@123");
            UsersPage usersPage = MainMenuPiece.ClickUsersIcon();
            NewUserPage newUserPage =  usersPage.ClickAddUserButton();

            newUserPage.EnterUsername(TestConfigValues.LocalSAUsername);
            newUserPage.EnterFirstName(TestConfigValues.LocalSAFirstName);
            newUserPage.EnterLastName(TestConfigValues.LocalSALastName);
            newUserPage.TypeAndSelectMasterEngagement(TestConfigValues.MasterEngagement1);
            newUserPage.EnterAdditionalUserInformation("Tester", "123_ABC", "Test user information +@+_!@(!*_465464");
            newUserPage.SelectTimeZone("(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna");
            newUserPage.SelectCulture("English (US)");
            newUserPage.SelectLanguage("English (UK)");
            newUserPage.ClickOnTab("Access and roles");
            newUserPage.AddEngagementInEngagementAndApplications(TestConfigValues.MasterEngagement1);
            newUserPage.ClickOnTab("Authentication");
            newUserPage.SetOverrideMasterEngagementSettingsON();
            EngagementsPage engagementsPage = newUserPage.ClickSave();
        }
    }
}
