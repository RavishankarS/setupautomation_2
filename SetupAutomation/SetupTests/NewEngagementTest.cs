﻿using NUnit.Framework;
using SetupPageObjects;
using SetupPieceObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupTests
{
    [TestFixture]
    public class AddNewEngagementTest : SetupBaseTest
    {
        [Test]
        public void VerifyCreateNewEngagement()
        {
            //Login as adra super user
            LoginPage loginPage = new LoginPage();
            loginPage.LoginAs("sa@99xt.com", "Intel@123");
            EngagementsPage engagementsPage =   MainMenuPiece.ClickEngagementsIcon();
            NewEngagementPage newEngagementPage =  engagementsPage.ClickAddEngagementButton();

            //Thread.Sleep(10000);
            //newEngagementPage.WaitTillAllAjaxCalls();

            newEngagementPage.EnterEngagementName("Ravi Auto New Engagement");
            newEngagementPage.EnterCustomerId(121452);
            newEngagementPage.SelectTimeZone("(UTC-03:30) Newfoundland");
            newEngagementPage.setAppAccess("Balancer");
            newEngagementPage.selectTypeAndEdition("Balancer", "Production", "Enterprise");
            engagementsPage = newEngagementPage.clickSave();

            
        }
    }
}

