﻿using SetupPageObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetupTests
{
    [TestFixture]
    class LoginTest:SetupBaseTest
    {
        [Test]
        public void VerifyLogin()
        {
            Assert.AreEqual("Login", LoginPage.GetTitle());
            Assert.AreEqual("Log in to Adra", LoginPage.GetApplicationName());
            LoginPage loginPage = new LoginPage();
            loginPage.LoginAs("sa@99xt.com", "Intel@123");
            Assert.AreEqual("SETUP", HomePage.GetApplicationName());
        }
    }
}
