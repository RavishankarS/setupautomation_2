﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SetupPageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace SetupTests
{
    public class SetupBaseTest
    {
        [SetUp]
        public void Init()
        {
            Driver.Initialise();
            LoginPage.GoToUrl();
        }

        [TearDown]
        public void Shutdown()
        {
            Driver.Shutdown();
        }

        public void WaitTillAllAjaxCalls()
        {
            WebDriverWait wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(20));
            IJavaScriptExecutor jsScript = Driver.Instance as IJavaScriptExecutor;
            wait.Until((d) => (bool)jsScript.ExecuteScript("return jQuery.active == 0"));
        }

        public void HighlightElements()
        {
            var js = Driver.Instance as IJavaScriptExecutor;
            if (js != null)
            {
                IWebElement element = Driver.Instance.FindElement(By.Id("engagementName"));
                js.ExecuteScript("arguments[0].setAttribute(arguments[1], arguments[2])", element, "style", "border: 2px solid yellow; color: red; font-weight: bold;");
            }
        }
    }
}